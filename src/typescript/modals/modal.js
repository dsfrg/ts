"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.showModal = void 0;
var domHelper_1 = require("../helpers/domHelper");
function showModal(_a) {
    var title = _a.title, bodyElement = _a.bodyElement;
    var root = getModalContainer();
    var modal = createModal(title, bodyElement);
    root === null || root === void 0 ? void 0 : root.append(modal);
}
exports.showModal = showModal;
function getModalContainer() {
    return document.getElementById('root');
}
function createModal(title, bodyElement) {
    var layer = domHelper_1.createElement({ tagName: 'div', className: 'modal-layer' });
    var modalContainer = domHelper_1.createElement({ tagName: 'div', className: 'modal-root' });
    var header = createHeader(title);
    modalContainer.append(header, bodyElement);
    layer.append(modalContainer);
    return layer;
}
function createHeader(title) {
    var headerElement = domHelper_1.createElement({ tagName: 'div', className: 'modal-header' });
    var titleElement = domHelper_1.createElement({ tagName: 'span' });
    var closeButton = domHelper_1.createElement({ tagName: 'div', className: 'close-btn' });
    titleElement.innerText = title;
    closeButton.innerText = '×';
    closeButton.addEventListener('click', hideModal);
    headerElement.append(title, closeButton);
    return headerElement;
}
function hideModal(event) {
    var modal = document.getElementsByClassName('modal-layer')[0];
    modal === null || modal === void 0 ? void 0 : modal.remove();
}
