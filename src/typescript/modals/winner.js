"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.showWinnerModal = void 0;
var domHelper_1 = require("../helpers/domHelper");
var modal_1 = require("./modal");
function showWinnerModal(fighter) {
    var image = domHelper_1.createElement({ tagName: 'img', attributes: { src: fighter.source } });
    var name = domHelper_1.createElement({ tagName: 'p' });
    var block = domHelper_1.createElement({ tagName: 'div' });
    name.innerText = "Congratulations to " + fighter.name;
    block.append(image, name);
    modal_1.showModal({ title: 'Winner', bodyElement: block });
}
exports.showWinnerModal = showWinnerModal;
