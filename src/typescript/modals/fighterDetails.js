"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.showFighterDetailsModal = void 0;
var domHelper_1 = require("../helpers/domHelper");
var modal_1 = require("./modal");
function showFighterDetailsModal(fighter) {
    var title = 'Fighter info';
    var bodyElement = createFighterDetails(fighter);
    modal_1.showModal({ title: title, bodyElement: bodyElement });
}
exports.showFighterDetailsModal = showFighterDetailsModal;
function createFighterDetails(fighter) {
    var fighterDetails = domHelper_1.createElement({ tagName: 'div', className: 'modal-body' });
    var nameElement = domHelper_1.createElement({ tagName: 'span', className: 'fighter-name' });
    var attackElement = domHelper_1.createElement({ tagName: 'p' });
    var defenseElement = domHelper_1.createElement({ tagName: 'p' });
    var healthElement = domHelper_1.createElement({ tagName: 'p' });
    var imgElement = domHelper_1.createElement({ tagName: 'img', attributes: { src: fighter.source } });
    var brElement = domHelper_1.createElement({ tagName: 'br' });
    nameElement.innerText = fighter.name;
    attackElement.innerText = fighter.attack + " - " + 'attack';
    defenseElement.innerText = fighter.defense + " - " + 'defense';
    healthElement.innerText = fighter.health + " - " + 'health';
    fighterDetails.append(nameElement, brElement, attackElement, brElement, defenseElement, brElement, healthElement, brElement, imgElement);
    return fighterDetails;
}
