"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeFightField = exports.appendTimelineItem = exports.getFightField = void 0;
var domHelper_1 = require("./helpers/domHelper");
function getFightField(nameField, firstFighter, secondFighter) {
    var fightField = domHelper_1.createElement({ tagName: 'div', className: 'fightField' });
    var textField = domHelper_1.createElement({ tagName: 'div', className: 'fightTitle' });
    var headline = domHelper_1.createElement({ tagName: 'h4', className: 'fightHeadline' });
    headline.innerText = nameField;
    textField.appendChild(headline);
    var figtersContainer = domHelper_1.createElement({ tagName: 'div', className: 'figtersContainer' });
    var firstFighterCard = domHelper_1.createElement({ tagName: 'div', className: 'fighterCard' });
    var firstFighterImg = domHelper_1.createElement({ tagName: 'img', className: 'fighterImg', attributes: { src: firstFighter.source } });
    var attackElement = domHelper_1.createElement({ tagName: 'p' });
    var defenseElement = domHelper_1.createElement({ tagName: 'p' });
    var healthElement = domHelper_1.createElement({ tagName: 'p' });
    var nameElement = domHelper_1.createElement({ tagName: 'p' });
    ;
    attackElement.innerText = firstFighter.attack + " - " + 'attack';
    defenseElement.innerText = firstFighter.defense + " - " + 'defense';
    healthElement.innerText = firstFighter.health + " - " + 'health';
    nameElement.innerText = firstFighter.name + " - name";
    //configure first card
    firstFighterCard.appendChild(firstFighterImg);
    firstFighterCard.appendChild(attackElement);
    firstFighterCard.appendChild(defenseElement);
    firstFighterCard.appendChild(healthElement);
    firstFighterCard.appendChild(nameElement);
    var secondFighterCard = domHelper_1.createElement({ tagName: 'div', className: 'fighterCard' });
    var secondFighterImg = domHelper_1.createElement({ tagName: 'img', className: 'fighterImg', attributes: { src: secondFighter.source } });
    var attackElement2 = domHelper_1.createElement({ tagName: 'p' });
    var defenseElement2 = domHelper_1.createElement({ tagName: 'p' });
    var healthElement2 = domHelper_1.createElement({ tagName: 'p' });
    var nameElement2 = domHelper_1.createElement({ tagName: 'p' });
    ;
    attackElement2.innerText = secondFighter.attack + " - " + 'attack';
    defenseElement2.innerText = secondFighter.defense + " - " + 'defense';
    healthElement2.innerText = secondFighter.health + " - " + 'health';
    nameElement2.innerText = secondFighter.name + " - name";
    //configure second card
    secondFighterCard.appendChild(secondFighterImg);
    secondFighterCard.appendChild(attackElement2);
    secondFighterCard.appendChild(defenseElement2);
    secondFighterCard.appendChild(healthElement2);
    secondFighterCard.appendChild(nameElement2);
    //timeline
    var timeline = domHelper_1.createElement({ tagName: 'div', className: 'timeline' });
    figtersContainer.appendChild(firstFighterCard);
    figtersContainer.appendChild(timeline);
    figtersContainer.appendChild(secondFighterCard);
    fightField.appendChild(textField);
    fightField.appendChild(figtersContainer);
    return fightField;
}
exports.getFightField = getFightField;
function appendTimelineItem(text) {
    var timeline = document.getElementsByClassName('timeline');
    var p = domHelper_1.createElement({ tagName: 'p' });
    p.innerText = text;
    timeline[0].appendChild(p);
}
exports.appendTimelineItem = appendTimelineItem;
function removeFightField() {
    var field = document.getElementsByClassName('fightField');
    if (field.length === 0)
        return;
    var body = document.getElementsByTagName('body');
    body[0].removeChild(field[0]);
}
exports.removeFightField = removeFightField;
