"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBlockPower = exports.getHitPower = exports.getDamage = exports.fight = void 0;
var fightView_1 = require("./fightView");
function fight(firstFighter, secondFighter) {
    fightView_1.removeFightField(); //remove old fight field if exists
    var field = fightView_1.getFightField(firstFighter.name + " VS " + secondFighter.name, firstFighter, secondFighter);
    var body = document.getElementsByTagName('body');
    body[0].appendChild(field);
    while (true) {
        var damageToFirst = getDamage(secondFighter, firstFighter);
        var damageToSecond = getDamage(firstFighter, secondFighter);
        firstFighter.health -= damageToFirst;
        secondFighter.health -= damageToSecond;
        fightView_1.appendTimelineItem(secondFighter.name + " give damage to " + firstFighter.name + ": " + damageToFirst);
        fightView_1.appendTimelineItem(firstFighter.name + " give damage to " + secondFighter.name + ": " + damageToSecond);
        if (firstFighter.health <= 0 || secondFighter.health <= 0)
            break;
    }
    if (secondFighter.health <= 0)
        return firstFighter;
    else
        return secondFighter;
}
exports.fight = fight;
function getDamage(attacker, enemy) {
    var hitPower = getHitPower(attacker);
    var blockPower = getBlockPower(enemy);
    if (hitPower - blockPower > 0)
        return hitPower - blockPower;
    else
        return 0;
}
exports.getDamage = getDamage;
function getHitPower(fighter) {
    var power = fighter.attack * (Math.random() + 1);
    return power;
}
exports.getHitPower = getHitPower;
function getBlockPower(fighter) {
    var power = fighter.defense * (Math.random() + 1);
    return power;
}
exports.getBlockPower = getBlockPower;
