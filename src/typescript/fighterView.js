"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createFighter = void 0;
var domHelper_1 = require("./helpers/domHelper");
function createFighter(fighter, handleClick, selectFighter) {
    var name = fighter.name, source = fighter.source;
    var nameElement = createName(name);
    var imageElement = createImage(source);
    var checkboxElement = createCheckbox();
    var fighterContainer = domHelper_1.createElement({ tagName: 'div', className: 'fighter' });
    fighterContainer.append(imageElement, nameElement, checkboxElement);
    var preventCheckboxClick = function (ev) { return ev.stopPropagation(); };
    var onCheckboxClick = function (ev) { return selectFighter(ev, fighter); };
    var onFighterClick = function (ev) { return handleClick(ev, fighter); };
    fighterContainer.addEventListener('click', onFighterClick, false);
    checkboxElement.addEventListener('change', onCheckboxClick, false);
    checkboxElement.addEventListener('click', preventCheckboxClick, false);
    return fighterContainer;
}
exports.createFighter = createFighter;
function createName(name) {
    var nameElement = domHelper_1.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;
    return nameElement;
}
function createImage(source) {
    var attributes = { src: source };
    var imgElement = domHelper_1.createElement({ tagName: 'img', className: 'fighter-image', attributes: attributes });
    return imgElement;
}
function createCheckbox() {
    var label = domHelper_1.createElement({ tagName: 'label', className: 'custom-checkbox' });
    var span = domHelper_1.createElement({ tagName: 'span', className: 'checkmark' });
    var attributes = { type: 'checkbox' };
    var checkboxElement = domHelper_1.createElement({ tagName: 'input', attributes: attributes });
    label.append(checkboxElement, span);
    return label;
}
