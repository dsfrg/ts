"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createElement = void 0;
function createElement(_a) {
    var tagName = _a.tagName, className = _a.className, _b = _a.attributes, attributes = _b === void 0 ? {} : _b;
    var element = document.createElement(tagName);
    if (className) {
        element.classList.add(className);
    }
    Object.keys(attributes).forEach(function (key) { return element.setAttribute(key, attributes[key]); });
    return element;
}
exports.createElement = createElement;
