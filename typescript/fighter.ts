export interface Figter
{
    _id: number;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source: string;
}