import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { Figter } from './fighter';
import { getFighterDetails } from './services/fightersService';

export function createFighters(fighters: Figter[]): HTMLElement {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event: any, fighter: Figter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: number): Promise<Figter> {
  return getFighterDetails(fighterId);
}

function createFightersSelector() {
  const selectedFighters = new Map();

  return async function selectFighterForBattle(event: any, fighter: Figter) {
    const fullInfo = await getFighterInfo(fighter._id);
    if (event.target.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      var fightersToFight: Figter[] = Array.from(selectedFighters.values()); 

      const clonedFighters: Figter[] = []; //clone , cause fighter info is not correct after fight
      fightersToFight.forEach(val => clonedFighters.push(Object.assign({}, val)));

      const winner = fight(clonedFighters[0], clonedFighters[1]);
      showWinnerModal(winner);
      

      //if fight ends then clear all checkboxes
      selectedFighters.clear();
      var checkboxes = document.getElementsByTagName('input');
      for (let item = 0; item < checkboxes.length; item++) {
        (checkboxes[item] as HTMLInputElement).checked = false;
      }
      setTimeout(() => alert('Scroll down to see detailed result'), 1000);
    }
  }
}
