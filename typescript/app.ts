import { getFighters } from './services/fightersService'
import { createFighters } from './fightersView';
import { Figter } from './fighter';

const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');
export var fighters: Figter[];
export async function startApp() {
    if(loadingElement != null && rootElement != null)
        try {
            
            
            loadingElement.style.visibility = 'visible';
            var width = window.innerWidth;
            width = width * 0.9;
            var height = window.innerHeight;
            height = height * 0.9;
            
            fighters = await getFighters();
            const fightersElement = createFighters(fighters);

            rootElement.appendChild(fightersElement);
        } catch (error) {
            console.warn(error);
            rootElement.innerText = 'Failed to load data';
        } finally {
            loadingElement.style.visibility = 'hidden';
        }
}
