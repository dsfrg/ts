import { callApi } from '../helpers/apiHepler';
import { Figter } from '../fighter';

export async function getFighters(): Promise<Figter[]> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return new Promise<Figter[]>((resolve, reject) => {
        resolve(apiResult as Figter[])
    });
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: number) : Promise<Figter> {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');
    
    return new Promise<Figter>((resolve, reject) => {
        resolve(apiResult as Figter)
    });
}

