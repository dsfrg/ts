import { Figter } from "./fighter";
import { getFightField, appendTimelineItem, removeFightField } from "./fightView";
import { getFighterInfo } from "./fightersView";

export  function fight(firstFighter: Figter, secondFighter: Figter): Figter {
  removeFightField(); //remove old fight field if exists

  var field  = getFightField(`${firstFighter.name} VS ${secondFighter.name}`, firstFighter, secondFighter);
  var body = document.getElementsByTagName('body');
  body[0].appendChild(field);

  while(true)
  {
    var damageToFirst = getDamage(secondFighter, firstFighter);
    var damageToSecond = getDamage(firstFighter, secondFighter);

    firstFighter.health -= damageToFirst;
    secondFighter.health -= damageToSecond;

    appendTimelineItem(`${secondFighter.name} give damage to ${firstFighter.name}: ${damageToFirst}`);
    appendTimelineItem(`${firstFighter.name} give damage to ${secondFighter.name}: ${damageToSecond}`);

    if (firstFighter.health <= 0 || secondFighter.health <= 0) 
      break; 
  }

  if(secondFighter.health <= 0)
    return firstFighter;
  else return secondFighter;
}
  
  export function getDamage(attacker: Figter, enemy: Figter) {
    var hitPower = getHitPower(attacker);
    var blockPower = getBlockPower(enemy);
    if(hitPower - blockPower > 0)
      return hitPower - blockPower;
    else 
      return 0;
  }
  
  export function getHitPower(fighter: Figter) : number {
    var power = fighter.attack * (Math.random() + 1 );
    return power;
  }
  
  export function getBlockPower(fighter: Figter) : number {
    var power = fighter.defense * (Math.random() + 1 );
    return power;
  }
  