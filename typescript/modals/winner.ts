import { Figter } from "../fighter";
import { createElement } from "../helpers/domHelper";
import { showModal } from "./modal";

export  function showWinnerModal(fighter: Figter) {
    var image = createElement({tagName: 'img', attributes: {src: fighter.source}});
    var name = createElement({tagName: 'p'});
    var block = createElement({tagName: 'div'});

    name.innerText = `Congratulations to ${fighter.name}`;
    block.append(image, name);
    showModal({ title: 'Winner', bodyElement: block });
  }