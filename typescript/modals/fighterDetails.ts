import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { Figter } from '../fighter';

export  function showFighterDetailsModal(fighter: Figter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: Figter): HTMLElement {
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

  const attackElement = createElement({tagName: 'p'});
  const defenseElement = createElement({tagName: 'p'});
  const healthElement = createElement({tagName: 'p'});
  const imgElement = createElement({tagName: 'img', attributes: {src: fighter.source}});
  const brElement = createElement({tagName: 'br'});

  nameElement.innerText = fighter.name;
  attackElement.innerText = `${fighter.attack} - ` + 'attack';
  defenseElement.innerText = `${fighter.defense} - ` + 'defense';
  healthElement.innerText = `${fighter.health} - ` + 'health';
  fighterDetails.append(nameElement,brElement, attackElement, brElement, defenseElement, brElement, healthElement, brElement, imgElement);

  return fighterDetails;
}
