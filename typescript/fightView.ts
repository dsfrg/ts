import { createElement } from "./helpers/domHelper";
import { Figter } from "./fighter";

export function getFightField(nameField: string, firstFighter: Figter, secondFighter: Figter): HTMLElement {
    var fightField = createElement({tagName: 'div', className: 'fightField'});

    var textField = createElement({tagName: 'div', className: 'fightTitle'});
    var headline = createElement({tagName: 'h4', className: 'fightHeadline'});
    headline.innerText = nameField;
    textField.appendChild(headline);

    var figtersContainer = createElement({tagName: 'div', className: 'figtersContainer'});

    var firstFighterCard = createElement({tagName: 'div', className: 'fighterCard'});
    var firstFighterImg = createElement({tagName: 'img', className: 'fighterImg', attributes: {src: firstFighter.source}});
    const attackElement = createElement({tagName: 'p'});
    const defenseElement = createElement({tagName: 'p'});
    const healthElement = createElement({tagName: 'p'});
    const nameElement = createElement({tagName: 'p'});;
    attackElement.innerText = `${firstFighter.attack} - ` + 'attack';
    defenseElement.innerText = `${firstFighter.defense} - ` + 'defense';
    healthElement.innerText = `${firstFighter.health} - ` + 'health';
    nameElement.innerText = `${firstFighter.name} - name`;
    //configure first card
    firstFighterCard.appendChild(firstFighterImg);
    firstFighterCard.appendChild(attackElement);
    firstFighterCard.appendChild(defenseElement);
    firstFighterCard.appendChild(healthElement);
    firstFighterCard.appendChild(nameElement);


    var secondFighterCard = createElement({tagName: 'div', className: 'fighterCard'});
    var secondFighterImg = createElement({tagName: 'img', className: 'fighterImg', attributes: {src: secondFighter.source}});
    const attackElement2 = createElement({tagName: 'p'});
    const defenseElement2 = createElement({tagName: 'p'});
    const healthElement2 = createElement({tagName: 'p'});
    const nameElement2 = createElement({tagName: 'p'});;
    attackElement2.innerText = `${secondFighter.attack} - ` + 'attack';
    defenseElement2.innerText = `${secondFighter.defense} - ` + 'defense';
    healthElement2.innerText = `${secondFighter.health} - ` + 'health';
    nameElement2.innerText = `${secondFighter.name} - name`;
    //configure second card
    secondFighterCard.appendChild(secondFighterImg);
    secondFighterCard.appendChild(attackElement2);
    secondFighterCard.appendChild(defenseElement2);
    secondFighterCard.appendChild(healthElement2);
    secondFighterCard.appendChild(nameElement2);

    //timeline
    var timeline = createElement({tagName: 'div', className: 'timeline'})

    figtersContainer.appendChild(firstFighterCard);
    figtersContainer.appendChild(timeline);
    figtersContainer.appendChild(secondFighterCard);
    
    fightField.appendChild(textField);
    fightField.appendChild(figtersContainer);

    return fightField;
}


export function appendTimelineItem(text: string): void {
    var timeline = document.getElementsByClassName('timeline');
    var p = createElement({tagName: 'p'});
    p.innerText = text;

    timeline[0].appendChild(p);
}

export function removeFightField() {
    var field = document.getElementsByClassName('fightField');
    if(field.length === 0)
        return;
    var body = document.getElementsByTagName('body');
    body[0].removeChild(field[0]);
}